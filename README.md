# 构建Delaunay三角网

#### 介绍
对二维平面上的散列点构建最大凸包，然后在采用逐点插入法构建Delaunay三角网。

#### 说明 
1.黑色点为随机散列点。<br>
2.蓝色点为构建的最大凸包点集。<br>
3.蓝色边为构建的三角网<br>

#### 构成Delaunay三角网示例图
<div style="display:flex; height:auto; justify-content: center;">
<img src="https://gitee.com/xudaile/delaunay_create/raw/master/images/三角网.png"/>
<p>三角网外接圆检测</p>
<img src="https://gitee.com/xudaile/delaunay_create/raw/master/images/三角网外接圆检测.png"/>
<p>中国省份构建三角网</p>
<img src="https://gitee.com/xudaile/delaunay_create/raw/master/images/中国省.png"/>
<img src="https://gitee.com/xudaile/delaunay_create/raw/master/images/china.png"/>
</div>