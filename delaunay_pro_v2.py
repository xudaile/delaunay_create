from sys import exit
import random
import pygame
import math

# 设置游戏屏幕大小
SCREEN_WIDTH = 1500
SCREEN_HEIGHT = 900

pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
# 游戏界面标题
pygame.display.set_caption('三角网')

# 游戏循环帧率设置
clock = pygame.time.Clock()

# Define the colors we will use in RGB format
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

# 判断游戏循环退出的参数
running = True

# 是否创建点位信息
is_create_point = False
points = []

# Delaunay 三角形集
triangle_list = []

# temp Delaunay 临时三角形集
temp_triangle_list = []

# 三角构建完成
triangle_is_finished = False


# edge buffer 临时边集
edge_buffer = []

# 构成的最大凸包点集
zz_list = []


# 平面二维坐标点类
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


# 三角形类 1个三角形由3个点构成 形成3条边
class Triangle:
    def __init__(self):
        pass

    def __init__(self, one, two, three):
        self.one = one
        self.two = two
        self.three = three
        self.edge_three = Edge(self.two, self.three)
        self.edge_one = Edge(self.one, self.two)
        self.edge_two = Edge(self.one, self.three)


# 边类 1条边由2个点构成
class Edge:
    def __init__(self, one, two):
        self.one = one
        self.two = two


# 圆类 1个圆由1个圆心点 + 1个半径构成
class Circle:
    def __init__(self, center, r):
        self.center = center
        self.r = r


# 创建point 随机点位
def draw_points():
    global is_create_point
    if not is_create_point:
        for i in range(20):
            x = random.randint(SCREEN_WIDTH // 6, SCREEN_WIDTH - 200)
            y = random.randint(SCREEN_HEIGHT // 6, SCREEN_HEIGHT - 200)
            points.append(Point(x, y))
        # 固定测试坐标点数据 [可忽略]
        # points.append(Point(321, 251))
        # points.append(Point(351, 576))
        # points.append(Point(357, 395))
        # points.append(Point(453, 195))
        # points.append(Point(519, 244))
        # points.append(Point(528, 278))
        # points.append(Point(562, 261))
        # points.append(Point(600, 496))
        # points.append(Point(766, 456))
        # points.append(Point(793, 680))
        # points.append(Point(819, 262))
        # points.append(Point(839, 477))
        # points.append(Point(875, 419))
        # points.append(Point(910, 296))
        # points.append(Point(915, 459))
        # points.append(Point(920, 344))
        # points.append(Point(1006, 275))
        # points.append(Point(1012, 465))
        # points.append(Point(1044, 497))
        # points.append(Point(1048, 645))

        # 采用冒泡算法对x排序
        maopao_sort()
        for point in points:
            print(point.x, point.y)
        is_create_point = True
        # 最大凸包点
        convex_hull()
    # 画随机生成的点 颜色黑色
    for point in points:
        pygame.draw.circle(screen, BLACK, (point.x, point.y), 5)
    # 画凸包点 颜色蓝色
    for point in zz_list:
        pygame.draw.circle(screen, BLUE, (point.x, point.y), 5)


# 初始化三角网
def init_delaunay():
    global triangle_is_finished
    if triangle_is_finished:
        return
    else:
        triangle_is_finished = True
        convex_hull_insert()


# 基于递归组合算法实现
def combination(data_list, start_index, end_index, result_list):
    index = start_index + 1
    while index < end_index:
        result_list.append([data_list[start_index], data_list[index]])
        index += 1
    start_index += 1
    if start_index >= end_index:
        return result_list
    else:
        return combination(data_list, start_index, end_index, result_list)


# 凸包插入法实现
def convex_hull_insert():
    _points = points[:]
    # 在points中remove掉最大凸包点
    for point in zz_list:
        _points.remove(point)
    global triangle_list, temp_triangle_list
    for point in _points:
        if len(temp_triangle_list) == 0:
            for i in range(len(zz_list)):
                k = i+1
                if k >= len(zz_list):
                    k = 0
                triangle = Triangle(zz_list[i], zz_list[k], point)
                temp_triangle_list.append(triangle)
            continue
        _temp_triangle_list = temp_triangle_list[:]
        # 清空 edge_buffer
        edge_buffer.clear()
        for tem_triangle in _temp_triangle_list:
            a, b, r = get_circle(tem_triangle.one, tem_triangle.two, tem_triangle.three)
            circle = Circle(Point(a, b), r)
            distance = get_distance(circle.center, point)
            # 点在圆内
            if distance <= circle.r:
                # 三边添加到 edge_buffer
                edge_buffer.append(tem_triangle.edge_one)
                edge_buffer.append(tem_triangle.edge_two)
                edge_buffer.append(tem_triangle.edge_three)
                # 在 tem_triangle 中移除
                temp_triangle_list.remove(tem_triangle)
                continue
            # 点在圆的右侧
            if distance > circle.r and (circle.r + circle.center.x) <= point.x:
                # 添加到 delaunay triangle
                triangle_list.append(tem_triangle)
                # 在 tem_triangle 中移除
                temp_triangle_list.remove(tem_triangle)
                continue
            # if distance > circle.r and (circle.r + circle.center.x) > point.x:
            #     continue
        # 重复边
        duplicated = []
        # 每条边的坐标点集
        buffer_points = []
        for edge in edge_buffer:
            buffer_points.append((edge.one, edge.two))
        for i in range(len(edge_buffer)):
            if buffer_points.count((edge_buffer[i].one, edge_buffer[i].two)) > 1:
                duplicated.append(edge_buffer[i])
        # edge buffer进行去重
        edge_buffer_set = set()
        for edge in edge_buffer:
            edge_buffer_set.add(edge)
        edge_buffer_set_v2 = set(edge_buffer_set)
        # 在 edge buffer进行去重
        for edge in edge_buffer_set_v2:
            if edge in duplicated:
                edge_buffer_set.remove(edge)
        # 组成三个三角形，加入到temp triangles中
        for edge in edge_buffer_set:
            _triangle = Triangle(edge.one, edge.two, point)
            temp_triangle_list.append(_triangle)

    # temp_triangle_list 和 triangle_list 合并
    for temp_triangle in temp_triangle_list:
        triangle_list.append(temp_triangle)

    print('-------triangle_list size :' + str(len(triangle_list)))


# 求2点之间的距离
def get_distance(p1, p2):
    return math.sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y))


# 采用冒泡算法排序对x排序
def maopao_sort():
    for i in range(len(points)-1):
        for j in range(len(points)-i-1):
            if points[j].x > points[j+1].x:
                temp = points[j]
                points[j] = points[j+1]
                points[j+1] = temp


def get_minx_point():
    min_x_point = points[0]
    min_x = min_x_point[0]
    for i in range(1, len(points)):
        if points[i].x < min_x.x:
            min_x = points[i][0]
            min_x_point = points[i]
    return min_x_point


# 获取最小y的point
def get_miny_point():
    min_y_point = points[0]
    min_y = min_y_point.y
    for i in range(1, len(points)):
        if points[i].y < min_y:
            min_y = points[i].y
            min_y_point = points[i]
    return min_y_point


# 获取最大y的point
def get_maxy_point():
    max_y_point = points[0]
    max_y = max_y_point.y
    for i in range(1, len(points)):
        if points[i].y > max_y:
            max_y = points[i].y
            max_y_point = points[i]
    return max_y_point


# 画已存在的三角形 就是构成的三角网
def draw_delaunay():
    if len(triangle_list) < 1:
        return
    for triangle in triangle_list:
        pointlist = ((triangle.one.x, triangle.one.y), (triangle.two.x, triangle.two.y), (triangle.three.x, triangle.three.y))
        # 画线
        pygame.draw.lines(screen, BLUE, True, pointlist)
        # a, b, r = get_circle(triangle.one, triangle.two, triangle.three)
        # # 画外接圆 验证构造三角网使用
        # pygame.draw.circle(screen, RED, (int(a), int(b)), int(r), 1)


def area(a, b, c):
    return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)


# 求凸包
def convex_hull():
    maxy_point = get_maxy_point()
    _points = points[:]
    _points.remove(maxy_point)
    # 坐标点余弦值集
    cos_list = []
    # 余弦定理
    for point in _points:
        c = get_distance(maxy_point, point)
        b = maxy_point.y - point.y
        a = math.sqrt(c*c - b*b)
        t = (a*a+c*c-b*b)/(2*a*c)
        cos = math.degrees(math.acos(t))
        if point.x <= maxy_point.x:
            cos = 180-cos
        cos_list.append({'point': point, 'cos': cos})
    # 采用冒泡排序对余弦值cos排序
    for i in range(len(cos_list)-1):
        for j in range(len(cos_list)-i-1):
            if cos_list[j]['cos'] > cos_list[j+1]['cos']:
                temp = cos_list[j]
                cos_list[j] = cos_list[j+1]
                cos_list[j+1] = temp
    # 排序后的点不包含原点
    sort_point_list = []
    for cos in cos_list:
        sort_point_list.append(cos['point'])
    z_list = [maxy_point]
    # Graham扫描法 Graham扫描的思想是先找到凸包上的一个点，然后从那个点开始按逆时针方向逐个找凸包上的点，实际上就是进行极角排序，然后对其查询使用
    for i in range(len(sort_point_list)):
        if i == 0 or i == 1:
            z_list.append(sort_point_list[i])
        else:
            point = sort_point_list[i]
            _area = area(z_list[len(z_list)-2], z_list[len(z_list)-1], point)
            if _area > 0:
                z_list.remove(z_list[len(z_list)-1])
                z_list.append(point)
            else:
                z_list.append(point)
        # 再判断一下栈内最后的三个点是不是构成凸包 （优化的处理方法）
        while True:
            _area = area(z_list[len(z_list) - 3], z_list[len(z_list) - 2], z_list[len(z_list) - 1])
            if _area > 0:
                z_list.remove(z_list[len(z_list) - 2])
            else:
                break
    global zz_list
    zz_list = z_list[:]


def get_circle(p1, p2, p3):
    y2 = p2.y
    y1 = p1.y
    y3 = p3.y
    x1 = p1.x
    x2 = p2.x
    x3 = p3.x
    a = ((y2 - y1) * (y3 * y3 - y1 * y1 + x3 * x3 - x1 * x1) - (y3 - y1) * (y2 * y2 - y1 * y1 + x2 * x2 - x1 * x1)) / (
                2.0 * ((x3 - x1) * (y2 - y1) - (x2 - x1) * (y3 - y1)))
    b = ((x2 - x1) * (x3 * x3 - x1 * x1 + y3 * y3 - y1 * y1) - (x3 - x1) * (x2 * x2 - x1 * x1 + y2 * y2 - y1 * y1)) / (
                2.0 * ((y3 - y1) * (x2 - x1) - (y2 - y1) * (x3 - x1)))
    r2 = math.sqrt((x1 - a) * (x1 - a) + (y1 - b) * (y1 - b))
    return a, b, r2


while running:
    # 控制游戏最大帧率为60
    clock.tick(60)
    screen.fill((255, 255, 255))

    # 画随机坐标点
    draw_points()

    # 初始化三角网
    init_delaunay()

    # 画Delaunay三角网
    draw_delaunay()

    pygame.display.update()
    for event in pygame.event.get():
        # 检测 KEYDOWN 事件: KEYDOWN 是 pygame.locals 中定义的常量，pygame.locals文件开始已经导入
        if event.type == pygame.KEYDOWN:
            # 如果按下 Esc 那么主循环终止
            if event.key == pygame.K_ESCAPE:
                running = False
        # 检测 QUIT : 如果 QUIT, 终止主循环
        elif event.type == pygame.QUIT:
            running = False
            pygame.quit()
            exit()

# 显示得分并处理游戏退出
while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
    pygame.display.update()
